use reqwest::Error;
use serde::Deserialize;
use std::collections::HashSet;
use url::Url;

// Define the data structures to deserialize the JSON response
#[derive(Debug, Deserialize)]
struct Response {
    meta: Meta,
    results: Results,
}

#[derive(Debug, Deserialize)]
struct Meta {
    total: usize,
}

#[derive(Debug, Deserialize)]
struct Results {
    entities: Vec<Entity>,
}

#[derive(Debug, Deserialize)]
struct Entity {
    url: Url,
}

// Asynchronous function to perform the API request and extract unique URLs
async fn get_request() -> Result<(), Error> {
    // Create a new reqwest::Client instance for making HTTP requests
    let client = reqwest::Client::new();
    //let client = reqwest::Client::builder().build()?;

    // Initialize the offset to start from the first page
    let mut offset = 0;

    // Create an empty HashSet to store the unique URLs
    let mut url_set: HashSet<String> = HashSet::new();
    let mut total_urls: usize;
    //let mut total_urls = 0; // Variable to store the total number of URLs
    let mut pages: usize;
    //let mut pages = 0;

    // Loop to fetch data from multiple pages until there are no more pages left
    loop {
        
        eprintln!("{}", offset);
        
        // Perform the actual execution of the network request with the current offset
        let res = client
            .get("https://lupo-cloud.de/gmb/search/v1/search")
            .query(&[("q", ""), ("se", "st"), ("page[offset]", offset.to_string().as_str()),("page[limit]", "100")])
            .send()
            .await?
            .error_for_status()?
            .json::<Response>()
            .await?;

        // Extract and store the URLs in the HashSet from the entities on the current page
        for entity in &res.results.entities {
            if let Some(host) = entity.url.host_str() {
            url_set.insert(host.to_owned());
            }
        }


        // Update the total number of URLs

        total_urls = res.meta.total;
        pages = (total_urls + 100 - 1)/100;

        // Update the offset to fetch data from the next page
        offset += 1;

        // Check if there are more pages to fetch
        if offset >= pages - 360 {
        //if offset >= pages {
            //if offset >= res.meta.total {
            // If we have fetched all the desired pages (e.g., 501 pages), exit the loop
            break;
        }
    }

    // Now you have a HashSet containing the unique URLs from all pages

    // Print each URL in the HashSet
    for url in &url_set {
        println!("{}", url);
    }

    // Print the total number of URLs
    eprintln!("Total URLs: {}", total_urls);

    // Indicate that the function execution is successful
    Ok(())
}

// Asynchronous main function
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Call the get_request function to start fetching data and extracting URLs
    get_request().await
}
